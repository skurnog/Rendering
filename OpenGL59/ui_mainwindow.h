/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <widgetopengl.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QSlider *horizontalSlider_Slider;
    WidgetOpenGL *widgetOpenGL;
    QGroupBox *groupBoxView;
    QHBoxLayout *horizontalLayout;
    QSlider *verticalSliderRotX;
    QSlider *verticalSliderRotZ;
    QSlider *verticalSliderRotY;
    QSpacerItem *horizontalSpacer;
    QSlider *verticalSliderZoom;
    QGroupBox *groupBoxLight;
    QHBoxLayout *horizontalLayout_2;
    QSlider *verticalSliderLightX;
    QSlider *verticalSliderLightY;
    QSlider *verticalSliderLightZ;
    QGroupBox *groupBoxTex;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QSlider *verticalSliderTexNoiseFactor;
    QPushButton *pushButtonWood;
    QSlider *verticalSliderTexSize;
    QSlider *verticalSliderTexNoiseDens;
    QSlider *verticalSliderTexPow;
    QPushButton *pushButtonMarble;
    QSlider *verticalSliderTexNoiseFractal;
    QLabel *label_4;
    QLabel *label_5;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(883, 595);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalSlider_Slider = new QSlider(centralWidget);
        horizontalSlider_Slider->setObjectName(QStringLiteral("horizontalSlider_Slider"));
        horizontalSlider_Slider->setMaximum(10000);
        horizontalSlider_Slider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(horizontalSlider_Slider, 1, 0, 1, 1);

        widgetOpenGL = new WidgetOpenGL(centralWidget);
        widgetOpenGL->setObjectName(QStringLiteral("widgetOpenGL"));
        widgetOpenGL->setMinimumSize(QSize(200, 200));

        gridLayout->addWidget(widgetOpenGL, 0, 0, 1, 1);

        groupBoxView = new QGroupBox(centralWidget);
        groupBoxView->setObjectName(QStringLiteral("groupBoxView"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBoxView->sizePolicy().hasHeightForWidth());
        groupBoxView->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(groupBoxView);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalSliderRotX = new QSlider(groupBoxView);
        verticalSliderRotX->setObjectName(QStringLiteral("verticalSliderRotX"));
        verticalSliderRotX->setMaximum(3600);
        verticalSliderRotX->setValue(200);
        verticalSliderRotX->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderRotX);

        verticalSliderRotZ = new QSlider(groupBoxView);
        verticalSliderRotZ->setObjectName(QStringLiteral("verticalSliderRotZ"));
        verticalSliderRotZ->setMaximum(3600);
        verticalSliderRotZ->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderRotZ);

        verticalSliderRotY = new QSlider(groupBoxView);
        verticalSliderRotY->setObjectName(QStringLiteral("verticalSliderRotY"));
        verticalSliderRotY->setMaximum(3600);
        verticalSliderRotY->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderRotY);

        horizontalSpacer = new QSpacerItem(8, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        verticalSliderZoom = new QSlider(groupBoxView);
        verticalSliderZoom->setObjectName(QStringLiteral("verticalSliderZoom"));
        verticalSliderZoom->setMaximum(1000);
        verticalSliderZoom->setValue(100);
        verticalSliderZoom->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderZoom);


        gridLayout->addWidget(groupBoxView, 0, 5, 2, 1);

        groupBoxLight = new QGroupBox(centralWidget);
        groupBoxLight->setObjectName(QStringLiteral("groupBoxLight"));
        sizePolicy.setHeightForWidth(groupBoxLight->sizePolicy().hasHeightForWidth());
        groupBoxLight->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(groupBoxLight);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalSliderLightX = new QSlider(groupBoxLight);
        verticalSliderLightX->setObjectName(QStringLiteral("verticalSliderLightX"));
        verticalSliderLightX->setMinimum(-100);
        verticalSliderLightX->setMaximum(100);
        verticalSliderLightX->setValue(10);
        verticalSliderLightX->setOrientation(Qt::Vertical);

        horizontalLayout_2->addWidget(verticalSliderLightX);

        verticalSliderLightY = new QSlider(groupBoxLight);
        verticalSliderLightY->setObjectName(QStringLiteral("verticalSliderLightY"));
        verticalSliderLightY->setMinimum(-100);
        verticalSliderLightY->setMaximum(100);
        verticalSliderLightY->setValue(30);
        verticalSliderLightY->setOrientation(Qt::Vertical);

        horizontalLayout_2->addWidget(verticalSliderLightY);

        verticalSliderLightZ = new QSlider(groupBoxLight);
        verticalSliderLightZ->setObjectName(QStringLiteral("verticalSliderLightZ"));
        verticalSliderLightZ->setMinimum(-100);
        verticalSliderLightZ->setMaximum(100);
        verticalSliderLightZ->setOrientation(Qt::Vertical);

        horizontalLayout_2->addWidget(verticalSliderLightZ);


        gridLayout->addWidget(groupBoxLight, 0, 6, 2, 1);

        groupBoxTex = new QGroupBox(centralWidget);
        groupBoxTex->setObjectName(QStringLiteral("groupBoxTex"));
        gridLayout_2 = new QGridLayout(groupBoxTex);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label = new QLabel(groupBoxTex);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(groupBoxTex);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_2, 0, 1, 1, 1);

        label_3 = new QLabel(groupBoxTex);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_3, 0, 2, 1, 1);

        verticalSliderTexNoiseFactor = new QSlider(groupBoxTex);
        verticalSliderTexNoiseFactor->setObjectName(QStringLiteral("verticalSliderTexNoiseFactor"));
        verticalSliderTexNoiseFactor->setMinimum(1);
        verticalSliderTexNoiseFactor->setMaximum(500);
        verticalSliderTexNoiseFactor->setValue(4);
        verticalSliderTexNoiseFactor->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(verticalSliderTexNoiseFactor, 1, 2, 1, 1);

        pushButtonWood = new QPushButton(groupBoxTex);
        pushButtonWood->setObjectName(QStringLiteral("pushButtonWood"));

        gridLayout_2->addWidget(pushButtonWood, 2, 0, 1, 6);

        verticalSliderTexSize = new QSlider(groupBoxTex);
        verticalSliderTexSize->setObjectName(QStringLiteral("verticalSliderTexSize"));
        verticalSliderTexSize->setMinimum(0);
        verticalSliderTexSize->setMaximum(1000);
        verticalSliderTexSize->setValue(143);
        verticalSliderTexSize->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(verticalSliderTexSize, 1, 0, 1, 1);

        verticalSliderTexNoiseDens = new QSlider(groupBoxTex);
        verticalSliderTexNoiseDens->setObjectName(QStringLiteral("verticalSliderTexNoiseDens"));
        verticalSliderTexNoiseDens->setMinimum(1);
        verticalSliderTexNoiseDens->setMaximum(160);
        verticalSliderTexNoiseDens->setValue(35);
        verticalSliderTexNoiseDens->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(verticalSliderTexNoiseDens, 1, 1, 1, 1);

        verticalSliderTexPow = new QSlider(groupBoxTex);
        verticalSliderTexPow->setObjectName(QStringLiteral("verticalSliderTexPow"));
        verticalSliderTexPow->setMinimum(1);
        verticalSliderTexPow->setMaximum(500);
        verticalSliderTexPow->setValue(17);
        verticalSliderTexPow->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(verticalSliderTexPow, 1, 4, 1, 1);

        pushButtonMarble = new QPushButton(groupBoxTex);
        pushButtonMarble->setObjectName(QStringLiteral("pushButtonMarble"));

        gridLayout_2->addWidget(pushButtonMarble, 3, 0, 1, 6);

        verticalSliderTexNoiseFractal = new QSlider(groupBoxTex);
        verticalSliderTexNoiseFractal->setObjectName(QStringLiteral("verticalSliderTexNoiseFractal"));
        verticalSliderTexNoiseFractal->setMaximum(8);
        verticalSliderTexNoiseFractal->setValue(1);
        verticalSliderTexNoiseFractal->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(verticalSliderTexNoiseFractal, 1, 3, 1, 1);

        label_4 = new QLabel(groupBoxTex);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_4, 0, 3, 1, 1);

        label_5 = new QLabel(groupBoxTex);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_5, 0, 4, 1, 1);


        gridLayout->addWidget(groupBoxTex, 0, 7, 2, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        groupBoxView->setTitle(QApplication::translate("MainWindow", "Widok", Q_NULLPTR));
        groupBoxLight->setTitle(QApplication::translate("MainWindow", "\305\232wiat\305\202o", Q_NULLPTR));
        groupBoxTex->setTitle(QApplication::translate("MainWindow", "Tekstura", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "s", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "d", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "f", Q_NULLPTR));
        pushButtonWood->setText(QApplication::translate("MainWindow", "Wood", Q_NULLPTR));
        pushButtonMarble->setText(QApplication::translate("MainWindow", "Marble", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "w", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "n", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
