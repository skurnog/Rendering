#ifndef MODEL_H
#define MODEL_H

#include <QString>
#include <QStringList>
#include <QVector>
#include <QMap>

#include <GL/gl.h>

class Model
{
public:
    Model(): v_cnt(0), vn_cnt(0), f_cnt(0), read_normals(false), v(0), vn(0), vt(0) {}
    ~Model() { }

    void readFile(QString fname, bool readNormals, float scale);

    QVector<float> getVertData() { return vertData; }
    int getVertDataStride() { return stride; }
    int getVertDataCount() { return f_cnt; }
    int getVertDataSize() { return 3*f_cnt*stride*sizeof(float); }

    GLuint EBO = -1;
    QVector<GLuint>* getIndices() { return &vertexIndexList;}
private:
    QStringList source;
    int v_cnt, vn_cnt, f_cnt, stride;

    bool read_normals;

    float *v, *vn, *vt;

    QMap<QString, GLuint> uniqueVertexIndices;
    QVector<GLfloat> vertData;
    QVector<GLuint> vertexIndexList;

    void count_items();
    void alloc_items();
    void parse_items(float scale);
    void print();
};

#endif // MODEL_H
