#include "model.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>

void Model::readFile(QString fname, bool readNormals, float scale)
{
    qDebug() << "Czytam '" << fname << "'...";

    read_normals = readNormals;

    QFile file(fname);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        throw QString("Nie moge otworzyc pliku '%1'").arg(fname);

    QTextStream ts(&file);
    source.clear();
    while (!ts.atEnd())
        source << ts.readLine();
    file.close();

    count_items();
    alloc_items();
    parse_items(scale);
}

void Model::count_items()
{
    v_cnt = vn_cnt = f_cnt = 0;
    for (int i = 0; i < source.count(); i++)
    {
        if (source[i].startsWith("v "))
            v_cnt++;
        else if (source[i].startsWith("vn "))
            vn_cnt++;
        else if (source[i].startsWith("f "))
            f_cnt++;
    }
    qDebug() << "vertices:" << v_cnt;
    qDebug() << "normals:" << vn_cnt;
    qDebug() << "faces:" << f_cnt;

}

void Model::alloc_items()
{
    v = new float[3*v_cnt];
    memset(v, 0, sizeof(float)*3*v_cnt);
    if (read_normals)
    {
      vn = new float[3*vn_cnt]();
      memset(vn, 0, sizeof(float)*3*vn_cnt);
    }

    stride = 3 + 3*int(read_normals);
}

void Model::parse_items(float scale)
{
    QString l;
    QStringList sl, sl2;

    // wierzcholki...
    int p = 0;
    for (int i = 0; i < source.count(); i++)
    {
        if (source[i].startsWith("v "))
        {
            l = source[i].mid(2).trimmed();
            sl = l.split(" ");
            v[3*p + 0] = sl[0].toFloat()*scale;
            v[3*p + 1] = sl[1].toFloat()*scale;
            v[3*p + 2] = sl[2].toFloat()*scale;
            p++;
        }
    }

    // normalne...
    if (read_normals)
    {
        int p = 0;
        for (int i = 0; i < source.count(); i++)
        {
            if (source[i].startsWith("vn "))
            {
                l = source[i].mid(3).trimmed();
                sl = l.split(" ");

                vn[3*p + 0] = sl[0].toFloat();
                vn[3*p + 1] = sl[1].toFloat();
                vn[3*p + 2] = sl[2].toFloat();
                p++;
            }
        }
    }

   // trojkaty...
   for (int i = 0; i < source.count(); i++)
   {
        if (source[i].startsWith("f "))
        {
            l = source[i].mid(2).trimmed();
            sl = l.split(" ");

            for (int j = 0; j < 3; j++)
            {
                sl2 = sl[j].split("/");
                while (sl2.count() < 3)
                    sl2.append("");

                int vi = sl2[0].toInt() - 1;
                int vni = read_normals ? sl2[2].toInt() - 1 : 0;
                int uniqueVertexIndex;

                QString uniqueVert(QString::number(vi));
                uniqueVert.append("/");
                uniqueVert.append(QString::number(vni));

                if(uniqueVertexIndices.contains(uniqueVert)) {
                    uniqueVertexIndex = uniqueVertexIndices[uniqueVert];
                } else {
                    uniqueVertexIndex = uniqueVertexIndices.keys().size();
                    uniqueVertexIndices.insert(uniqueVert, uniqueVertexIndex);
                    vertData.append(v[3*vi + 0]);
                    vertData.append(v[3*vi + 1]);
                    vertData.append(v[3*vi + 2]);
                    if (read_normals) {
                          vertData.append(vn[3*vni + 0]);
                          vertData.append(vn[3*vni + 1]);
                          vertData.append(vn[3*vni + 2]);
                    }
                }

                vertexIndexList.append(uniqueVertexIndex);
            }
        }
   }

   delete [] v;
   delete [] vn;
   delete [] vt;

   qDebug() << "Ok, model wczytany.";
}

void Model::print()
{
    qDebug() << "stride:" << stride;
    for (int i = 0; i < f_cnt; i++)
    {
        QString s = "";
        for (int j = 0; j < stride*3; j++)
            s += (j ? ", ": "") + QString::number(vertData[i*stride + j]);
        qDebug() << s;
    }
}

