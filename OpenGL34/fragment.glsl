#version 450

struct Light
{
    vec3 pos;
    vec3 color;
};

struct Material
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
    bool useTex;
};

in vec3 fragNormal;
in vec4 fragPos;
in vec2 fragTexCoor;

uniform vec3 eyePos;
uniform Light light[2];
uniform float far;
uniform Material material;

uniform sampler2D textureColor;
uniform samplerCube textureShadow[2];

uniform int slider;
uniform bool useShadow;

out vec4 color;

float inShadowPCF2(Light light, samplerCube textureShadow)
{
    vec3 sample_dir[] = vec3[]
    (
      vec3( 1, 1, 1), vec3( 1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1),
      vec3( 1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
      vec3( 1, 1, 0), vec3( 1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
      vec3( 1, 0, 1), vec3(-1, 0, 1), vec3( 1, 0, -1), vec3(-1, 0, -1),
      vec3( 0, 1, 1), vec3( 0, -1, 1), vec3( 0, -1, -1), vec3( 0, 1, -1)
    );


    if (int(gl_FragCoord.x) < slider)
        return 1.0;

    vec3 fragToLight = fragPos.xyz - light.pos;
    float depth = length(fragToLight);

    if (depth >= far)
        return 1.0;

    float bias = max(0.02 * (1.0 - dot(fragNormal, fragToLight)), 0.0001);
    float texel = 0.04;
    float shadow = 0.0;
    for (int i = 0; i < sample_dir.length; i++)
      {
        float closestDepth = texture(textureShadow, fragToLight + sample_dir[i]*texel).r * far;
        shadow +=  depth - bias  > closestDepth ? 1.0 : 0.0;
      }

    return 1.0 - shadow/sample_dir.length;
}

void main()
{
    color = vec4(0);
    int iterations = useShadow ? 2 : 1;
    for (int i = 0; i < iterations; i++) {
        vec3 texColor = material.useTex ? texture(textureColor, fragTexCoor).rgb : vec3(1.0, 1.0, 1.0);
        vec3 lightDir = normalize(light[useShadow ? i : 0].pos - fragPos.xyz);
        vec3 normal   = normalize(fragNormal);

        // ambient
        vec3 ambient = material.ambient*texColor;


        // diffuse
        vec3 diffuse = max(dot(normal, lightDir), 0.0)*light[useShadow ? i : 0].color*material.diffuse*texColor;

        // specular
        float specular_strength = 0.5;
        int specular_pow = 20;
        vec3 viewDir = normalize(eyePos - fragPos.xyz);
        vec3 reflDir = reflect(-lightDir, normal);
        vec3 specular = specular_strength*pow(max(dot(viewDir, reflDir), 0.0), material.shininess)*light[useShadow ? i : 0].color*material.specular;

        float shadow = useShadow ? inShadowPCF2(light[i], textureShadow[i]) : 1.0;
        color = color + vec4(ambient + shadow*(diffuse + specular), 1.0);
    }
}
