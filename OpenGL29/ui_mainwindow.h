/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <widgetopengl.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QLabel *labelW;
    QGroupBox *groupBoxView;
    QHBoxLayout *horizontalLayout;
    QSlider *verticalSliderRotX;
    QSlider *verticalSliderRotZ;
    QSlider *verticalSliderRotY;
    QSpacerItem *horizontalSpacer;
    QSlider *verticalSliderZoom;
    QGroupBox *groupBoxLight;
    QHBoxLayout *horizontalLayout_2;
    QSlider *verticalSliderLightX;
    QSlider *verticalSliderLightY;
    QSlider *verticalSliderLightZ;
    WidgetOpenGL *widgetOpenGL;
    QSlider *slider;
    QPushButton *pushButton;
    QSlider *sliderFramebufferW;
    QSlider *sliderFramebufferH;
    QLabel *labelH;
    QCheckBox *checkBox;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(883, 595);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        labelW = new QLabel(centralWidget);
        labelW->setObjectName(QStringLiteral("labelW"));

        gridLayout->addWidget(labelW, 1, 1, 1, 1);

        groupBoxView = new QGroupBox(centralWidget);
        groupBoxView->setObjectName(QStringLiteral("groupBoxView"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBoxView->sizePolicy().hasHeightForWidth());
        groupBoxView->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(groupBoxView);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalSliderRotX = new QSlider(groupBoxView);
        verticalSliderRotX->setObjectName(QStringLiteral("verticalSliderRotX"));
        verticalSliderRotX->setMaximum(3600);
        verticalSliderRotX->setValue(200);
        verticalSliderRotX->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderRotX);

        verticalSliderRotZ = new QSlider(groupBoxView);
        verticalSliderRotZ->setObjectName(QStringLiteral("verticalSliderRotZ"));
        verticalSliderRotZ->setMaximum(3600);
        verticalSliderRotZ->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderRotZ);

        verticalSliderRotY = new QSlider(groupBoxView);
        verticalSliderRotY->setObjectName(QStringLiteral("verticalSliderRotY"));
        verticalSliderRotY->setMaximum(3600);
        verticalSliderRotY->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderRotY);

        horizontalSpacer = new QSpacerItem(8, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        verticalSliderZoom = new QSlider(groupBoxView);
        verticalSliderZoom->setObjectName(QStringLiteral("verticalSliderZoom"));
        verticalSliderZoom->setMaximum(1000);
        verticalSliderZoom->setValue(100);
        verticalSliderZoom->setOrientation(Qt::Vertical);

        horizontalLayout->addWidget(verticalSliderZoom);


        gridLayout->addWidget(groupBoxView, 0, 4, 3, 1);

        groupBoxLight = new QGroupBox(centralWidget);
        groupBoxLight->setObjectName(QStringLiteral("groupBoxLight"));
        sizePolicy.setHeightForWidth(groupBoxLight->sizePolicy().hasHeightForWidth());
        groupBoxLight->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(groupBoxLight);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalSliderLightX = new QSlider(groupBoxLight);
        verticalSliderLightX->setObjectName(QStringLiteral("verticalSliderLightX"));
        verticalSliderLightX->setMinimum(-100);
        verticalSliderLightX->setMaximum(100);
        verticalSliderLightX->setValue(0);
        verticalSliderLightX->setOrientation(Qt::Vertical);

        horizontalLayout_2->addWidget(verticalSliderLightX);

        verticalSliderLightY = new QSlider(groupBoxLight);
        verticalSliderLightY->setObjectName(QStringLiteral("verticalSliderLightY"));
        verticalSliderLightY->setMinimum(-100);
        verticalSliderLightY->setMaximum(100);
        verticalSliderLightY->setValue(10);
        verticalSliderLightY->setOrientation(Qt::Vertical);

        horizontalLayout_2->addWidget(verticalSliderLightY);

        verticalSliderLightZ = new QSlider(groupBoxLight);
        verticalSliderLightZ->setObjectName(QStringLiteral("verticalSliderLightZ"));
        verticalSliderLightZ->setMinimum(-100);
        verticalSliderLightZ->setMaximum(100);
        verticalSliderLightZ->setOrientation(Qt::Vertical);

        horizontalLayout_2->addWidget(verticalSliderLightZ);


        gridLayout->addWidget(groupBoxLight, 0, 5, 3, 1);

        widgetOpenGL = new WidgetOpenGL(centralWidget);
        widgetOpenGL->setObjectName(QStringLiteral("widgetOpenGL"));
        widgetOpenGL->setMinimumSize(QSize(200, 200));

        gridLayout->addWidget(widgetOpenGL, 0, 0, 1, 2);

        slider = new QSlider(centralWidget);
        slider->setObjectName(QStringLiteral("slider"));
        slider->setMaximum(10000);
        slider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(slider, 2, 0, 1, 1);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 2, 1, 1, 1);

        sliderFramebufferW = new QSlider(centralWidget);
        sliderFramebufferW->setObjectName(QStringLiteral("sliderFramebufferW"));
        sliderFramebufferW->setMaximum(300);
        sliderFramebufferW->setValue(256);
        sliderFramebufferW->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(sliderFramebufferW, 1, 0, 1, 1);

        sliderFramebufferH = new QSlider(centralWidget);
        sliderFramebufferH->setObjectName(QStringLiteral("sliderFramebufferH"));
        sliderFramebufferH->setMaximum(300);
        sliderFramebufferH->setValue(256);
        sliderFramebufferH->setOrientation(Qt::Vertical);

        gridLayout->addWidget(sliderFramebufferH, 0, 2, 1, 1);

        labelH = new QLabel(centralWidget);
        labelH->setObjectName(QStringLiteral("labelH"));

        gridLayout->addWidget(labelH, 1, 2, 1, 1);

        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setChecked(true);

        gridLayout->addWidget(checkBox, 2, 2, 1, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        labelW->setText(QApplication::translate("MainWindow", "1024", Q_NULLPTR));
        groupBoxView->setTitle(QApplication::translate("MainWindow", "Widok", Q_NULLPTR));
        groupBoxLight->setTitle(QApplication::translate("MainWindow", "\305\232wiat\305\202o", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MainWindow", " Zapisz framebuffer ", Q_NULLPTR));
        labelH->setText(QApplication::translate("MainWindow", "1024", Q_NULLPTR));
        checkBox->setText(QApplication::translate("MainWindow", "h=w", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
