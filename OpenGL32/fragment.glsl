#version 450

in vec4 gl_FragCoord ;
in flat float useColor;

out vec4 color;

void main()
{
    if (useColor == 1.0) {
        color = vec4(0.44f, 0.34f, 0.12f, 1.0f);
        gl_FragDepth = gl_FragCoord.z * 0.99; //avoid flickering
    } else {
        color = vec4(1f, 1f, 1f, 1f);
        gl_FragDepth = gl_FragCoord.z;
    }
}
