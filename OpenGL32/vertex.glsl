#version 450

in vec3 position;

uniform mat4 m_matrix;
uniform mat4 v_matrix;

void main()
{
    gl_Position = v_matrix*m_matrix*vec4(position, 1.0);
}
