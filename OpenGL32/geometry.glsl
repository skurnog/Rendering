#version 450

layout (triangles) in;
layout (triangle_strip, max_vertices=256) out;

out vec4 fragPos;
out flat float useColor;

uniform mat4 p_matrix;

struct Point{
    vec4 glPosition;
} points[256];

int point_counter = 0;

int get_point(int ind) {
    if (ind < 3) return ind;
    else return ind + 2*(ind/3);
}

vec4 midpoint(int a_ind, int b_ind) {
    return (points[a_ind].glPosition + points[b_ind].glPosition)/2.;
}

void add_midtriangle(int a_ind, int b_ind, int c_ind)
{
    a_ind = get_point(a_ind);
    b_ind = get_point(b_ind);
    c_ind = get_point(c_ind);

    points[point_counter++].glPosition = midpoint(a_ind, b_ind); // degenerate triangle
    points[point_counter++].glPosition = midpoint(a_ind, b_ind);
    points[point_counter++].glPosition = midpoint(b_ind, c_ind);
    points[point_counter++].glPosition = midpoint(c_ind, a_ind);
    points[point_counter++].glPosition = midpoint(c_ind, a_ind); // degenerate triangle
}

void main()
{
    useColor = 0;

    // init
    for(int i=0; i<3; i++) points[point_counter++].glPosition = gl_in[i].gl_Position; // adding points from input
    points[point_counter++].glPosition = gl_in[2].gl_Position; // degenerate triangle

    add_midtriangle(0,1,2);

    add_midtriangle(0,3,5);
    add_midtriangle(1,4,3);
    add_midtriangle(2,5,4);

    for(int i = 0; i<point_counter; i++){
        gl_Position = p_matrix *  points[i].glPosition;
        EmitVertex();

        // first draw mesh triangle
        if(i == 3) {
            useColor = 1;
            EndPrimitive();
        }
        if((i-4)%  5 == 0) EndPrimitive();
    }
}
